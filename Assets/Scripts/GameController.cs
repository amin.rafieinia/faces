﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public GameObject faceHolder;
    [HideInInspector] public Vector2 top_initialPos;
    [HideInInspector] public Vector2 midTop_initialPos;
    [HideInInspector] public Vector2 mid_initialPos;
    [HideInInspector] public Vector2 midBottom_initialPos;
    [HideInInspector] public Vector2 bottom_initialPos;
    [HideInInspector] public int score;
    [HideInInspector] public float timeRemaining;
    [HideInInspector] public GameState gameState = GameState.Menu;

    [SerializeField] private List<Sprite> faceSprites_Top;
    [SerializeField] private List<Sprite> faceSprites_MidTop;
    [SerializeField] private List<Sprite> faceSprites_Mid;
    [SerializeField] private List<Sprite> faceSprites_MidBottom;
    [SerializeField] private List<Sprite> faceSprites_Bottom;

    private int topPartIndex, midTopPartIndex, midPartIndex, midBottomPartIndex, bottomPartIndex;
    private List<int> foundFacesIndex;
    private bool isUsertouched = false;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        foundFacesIndex = new List<int>();
        timeRemaining = Calibration.instance.wholeTime;
        top_initialPos = faceHolder.transform.GetChild(0).position;
        midTop_initialPos = faceHolder.transform.GetChild(1).position;
        mid_initialPos = faceHolder.transform.GetChild(2).position;
        midBottom_initialPos = faceHolder.transform.GetChild(3).position;
        bottom_initialPos = faceHolder.transform.GetChild(4).position;
    }

    void Update()
    {
        switch (gameState)
        {
            case GameState.Play:
                timeRemaining -= Time.deltaTime;
                UIController.instance.UpdateTime();
                if (isUsertouched == false)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        isUsertouched = true;
                        Tutorial.instance.StopTutorial();
                    }
                }
                CheckLose();
                break;

            case GameState.Menu:
                break;

            case GameState.MatchFace:
                break;

            default:
                break;
        }
    }

    public void Play()
    {
        gameState = GameState.Play;
        faceHolder.SetActive(true);
        InitializeFace();
        Tutorial.instance.PlayTutorial();
        score = 0;
        foundFacesIndex.Clear();
    }

    private void InitializeFace()
    {
        while (topPartIndex == midTopPartIndex && topPartIndex == midPartIndex && topPartIndex == midBottomPartIndex && topPartIndex == bottomPartIndex)
        {
            topPartIndex = Random.Range(0, faceSprites_Top.Count);
            midTopPartIndex = Random.Range(0, faceSprites_MidTop.Count);
            midPartIndex = Random.Range(0, faceSprites_Mid.Count);
            midBottomPartIndex = Random.Range(0, faceSprites_MidBottom.Count);
            bottomPartIndex = Random.Range(0, faceSprites_Bottom.Count);
        }


        faceHolder.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = faceSprites_Top[topPartIndex];
        faceHolder.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = faceSprites_MidTop[midTopPartIndex];
        faceHolder.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = faceSprites_Mid[midPartIndex];
        faceHolder.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = faceSprites_MidBottom[midBottomPartIndex];
        faceHolder.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = faceSprites_Bottom[bottomPartIndex];

        faceHolder.transform.GetChild(0).position = top_initialPos;
        faceHolder.transform.GetChild(1).position = midTop_initialPos;
        faceHolder.transform.GetChild(2).position = mid_initialPos;
        faceHolder.transform.GetChild(3).position = midBottom_initialPos;
        faceHolder.transform.GetChild(4).position = bottom_initialPos;

        if (gameState == GameState.MatchFace)
        {
            gameState = GameState.Play;
        }
        else
        {
            StartCoroutine(CheckMatchFace());
        }
    }

    public void ChangeFacePart(FacePart facePart, bool isNext)
    {
        switch (facePart)
        {
            case FacePart.Top:
                if (isNext)
                {
                    topPartIndex = (topPartIndex + 1) % faceSprites_Top.Count;
                    faceHolder.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = faceSprites_Top[topPartIndex];
                }
                else
                {
                    topPartIndex = (topPartIndex + faceSprites_Top.Count - 1) % faceSprites_Top.Count;
                    faceHolder.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = faceSprites_Top[topPartIndex];
                }
                ResetPosition(FacePart.Top);
                break;

            case FacePart.MidTop:
                if (isNext)
                {
                    midTopPartIndex = (midTopPartIndex + 1) % faceSprites_MidTop.Count;
                    faceHolder.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = faceSprites_MidTop[midTopPartIndex];
                }
                else
                {
                    midTopPartIndex = (midTopPartIndex + faceSprites_MidTop.Count - 1) % faceSprites_MidTop.Count;
                    faceHolder.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = faceSprites_MidTop[midTopPartIndex];
                }
                ResetPosition(FacePart.MidTop);
                break;

            case FacePart.Mid:
                if (isNext)
                {
                    midPartIndex = (midPartIndex + 1) % faceSprites_Mid.Count;
                    faceHolder.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = faceSprites_Mid[midPartIndex];
                }
                else
                {
                    midPartIndex = (midPartIndex + faceSprites_Mid.Count - 1) % faceSprites_Mid.Count;
                    faceHolder.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = faceSprites_Mid[midPartIndex];
                }
                ResetPosition(FacePart.Mid);
                break;

            case FacePart.MidBottom:
                if (isNext)
                {
                    midBottomPartIndex = (midBottomPartIndex + 1) % faceSprites_MidBottom.Count;
                    faceHolder.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = faceSprites_MidBottom[midBottomPartIndex];
                }
                else
                {
                    midBottomPartIndex = (midBottomPartIndex + faceSprites_MidBottom.Count - 1) % faceSprites_MidBottom.Count;
                    faceHolder.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = faceSprites_MidBottom[midBottomPartIndex];
                }
                ResetPosition(FacePart.MidBottom);
                break;

            case FacePart.Bottom:
                if (isNext)
                {
                    bottomPartIndex = (bottomPartIndex + 1) % faceSprites_Bottom.Count;
                    faceHolder.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = faceSprites_Bottom[bottomPartIndex];
                }
                else
                {
                    bottomPartIndex = (bottomPartIndex + faceSprites_Bottom.Count - 1) % faceSprites_Bottom.Count;
                    faceHolder.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = faceSprites_Bottom[bottomPartIndex];
                }
                ResetPosition(FacePart.Bottom);
                break;

            default:
                break;
        }

        StartCoroutine(CheckMatchFace());
        Tutorial.instance.StopTutorial();
    }

    public void ResetPosition(FacePart facePart)
    {
        switch (facePart)
        {
            case FacePart.Top:
                faceHolder.transform.GetChild(0).position = top_initialPos;
                break;
            case FacePart.MidTop:
                faceHolder.transform.GetChild(1).position = midTop_initialPos;
                break;
            case FacePart.Mid:
                faceHolder.transform.GetChild(2).position = mid_initialPos;
                break;
            case FacePart.MidBottom:
                faceHolder.transform.GetChild(3).position = midBottom_initialPos;
                break;
            case FacePart.Bottom:
                faceHolder.transform.GetChild(4).position = bottom_initialPos;
                break;
            default:
                break;
        }
    }

    private IEnumerator CheckMatchFace()
    {
        yield return new WaitForSeconds(Calibration.instance.checkWinDelay);

        if (topPartIndex == midTopPartIndex && topPartIndex == midPartIndex && topPartIndex == midBottomPartIndex && topPartIndex == bottomPartIndex)
        {
            gameState = GameState.MatchFace;

            if (foundFacesIndex.Contains(topPartIndex) == false)
            {
                foundFacesIndex.Add(topPartIndex);
                timeRemaining += Calibration.instance.timeReward_NewFace;
                UIController.instance.ShowBonusTime(Calibration.instance.timeReward_NewFace, true);
                score++;
                UIController.instance.LightCityUp();
            }
            else
            {
                timeRemaining += Calibration.instance.timeReward_AnyFace;
                UIController.instance.ShowBonusTime(Calibration.instance.timeReward_AnyFace, false);
            }

            yield return new WaitForSeconds(Calibration.instance.timeToShuffleDelay);

            if (score == 8)
            {
                gameState = GameState.Win;
                UIController.instance.MainUI();
                faceHolder.SetActive(false);
                isUsertouched = false;
                timeRemaining = Calibration.instance.wholeTime;
                Tutorial.instance.StopTutorial();
                for (int i = 0; i < 5; i++)
                {
                    faceHolder.transform.GetChild(i).GetComponent<Drag>().isDragging = false;
                }
            }
            else
            {
                InitializeFace();
            }
        }
    }

    private void CheckLose()
    {
        if (timeRemaining < 0.1f)
        {
            UIController.instance.MainUI();
            faceHolder.SetActive(false);
            gameState = GameState.Menu;
            isUsertouched = false;
            timeRemaining = Calibration.instance.wholeTime;
            Tutorial.instance.StopTutorial();
            for (int i = 0; i < 5; i++)
            {
                faceHolder.transform.GetChild(i).GetComponent<Drag>().isDragging = false;
            }
        }
    }
}

public enum FacePart
{
    Top,
    MidTop,
    Mid,
    MidBottom,
    Bottom
}

public enum GameState
{
    Play,
    Menu,
    MatchFace,
    Win
}
