﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public static Tutorial instance;
    [SerializeField] private float animationTime;
    [SerializeField] private AnimationCurve moveCurve;
    [SerializeField] private float deltaX;

    private bool isAnimating = false;
    private float timePassed = 0;
    private Vector2 initialPosition;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        initialPosition = transform.position;
        GetComponent<SpriteRenderer>().sortingOrder = 2;
    }

    void Update()
    {
        if (isAnimating)
        {
            timePassed += Time.deltaTime;

            if (timePassed > animationTime)
            {
                transform.position = initialPosition;
                timePassed = 0;
            }
            else
            {
                transform.position = new Vector2(initialPosition.x + deltaX * moveCurve.Evaluate(timePassed / animationTime) , transform.position.y);
            }
        }
    }

    public void PlayTutorial()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        isAnimating = true;
    }

    public void StopTutorial()
    {
        timePassed = 0;
        isAnimating = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
