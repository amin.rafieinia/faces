﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calibration : MonoBehaviour
{
    public static Calibration instance;

    [Range(0.001f, 5)] public float dragOffsetToNext;
    [Range(0, 2)] public float checkWinDelay;
    [Range(0, 2)] public float timeToShuffleDelay;
    [Range(0, 90)] public int wholeTime;
    [Range(0, 30)] public int timeReward_NewFace;
    [Range(0, 30)] public int timeReward_AnyFace;
    [Range(0, 2)] public float hideBonusTimeDelay;

    void Awake()
    {
        instance = this;
    }
}
