﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    [SerializeField] private GameObject timePanel;
    [SerializeField] private GameObject timeBonusPanel;
    [SerializeField] private GameObject rewardTextPanel;
    [SerializeField] private GameObject WinTextPanel;
    [SerializeField] private Text textTime;
    [SerializeField] private Text textBonusTime;
    [SerializeField] private Text rewardText;
    [SerializeField] private GameObject playBtn;
    [SerializeField] private GameObject cityPanel;

    void Awake()
    {
        instance = this;
    }

    public void UpdateTime()
    {
        textTime.text = Mathf.Floor(GameController.instance.timeRemaining).ToString();
    }

    public void PlayGame()
    {
        if (GameController.instance.gameState == GameState.Win)
        {

            WinTextPanel.SetActive(false);
        }

        GameController.instance.Play();
        timePanel.SetActive(true);
        playBtn.SetActive(false);

        for (int i = 0; i < cityPanel.transform.childCount; i++)
        {
            cityPanel.transform.GetChild(i).GetComponent<CityAnimation>().Reset();
        }
    }

    public void MainUI()
    {
        timePanel.SetActive(false);
        timeBonusPanel.SetActive(false);
        playBtn.SetActive(true);
        if (GameController.instance.gameState == GameState.Win)
        {
            WinTextPanel.SetActive(true);
        }
    }

    public void LightCityUp()
    {
        cityPanel.transform.GetChild(GameController.instance.score - 1).GetComponent<CityAnimation>().Animate();
    }

    public void ShowBonusTime(int reward, bool isNewFace)
    {
        timeBonusPanel.SetActive(true);
        rewardTextPanel.SetActive(true);
        textBonusTime.text = string.Format("+{0}", reward.ToString());
        if (isNewFace)
        {
            rewardText.text = "NEW FACE!";
        }
        else
        {
            rewardText.text = "REPEATED FACE!";
        }
        StartCoroutine(HideBonusTime());
    }

    private IEnumerator HideBonusTime()
    {
        yield return new WaitForSeconds(Calibration.instance.hideBonusTimeDelay);
        timeBonusPanel.SetActive(false);
        rewardTextPanel.SetActive(false);
    }
}
