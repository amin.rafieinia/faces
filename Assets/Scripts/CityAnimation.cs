﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityAnimation : MonoBehaviour
{
    [SerializeField] private float yPosChange;
    [SerializeField] private AnimationCurve yChangeCurve;
    [SerializeField] private float animationTime;

    private bool isAnimating = false;
    private float timePassed = 0;
    private float initialYPos, finalYPos;

    void Start()
    {
        initialYPos = GetComponent<RectTransform>().position.y;
        finalYPos = initialYPos + yPosChange;
    }

    void Update()
    {
        if (isAnimating)
        {
            timePassed += Time.deltaTime;

            if (timePassed > animationTime)
            {
                GetComponent<RectTransform>().position = new Vector2(GetComponent<RectTransform>().position.x, finalYPos);
                isAnimating = false;
                timePassed = 0;
            }
            else
            {
                GetComponent<RectTransform>().position
                    = new Vector2(GetComponent<RectTransform>().position.x, initialYPos + yPosChange * yChangeCurve.Evaluate(timePassed / animationTime));
            }
        }
    }

    public void Animate()
    {
        isAnimating = true;
    }

    public void Reset()
    {
        GetComponent<RectTransform>().position = new Vector2(GetComponent<RectTransform>().position.x, initialYPos);
    }
}
