﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    [SerializeField] private FacePart facePart;
    [HideInInspector] public bool isDragging;

    public void OnMouseDown()
    {
        if (GameController.instance.gameState == GameState.Play)
        {
            isDragging = true;
        }
    }

    public void OnMouseUp()
    {
        if (transform.position.x - GameController.instance.top_initialPos.x > Calibration.instance.dragOffsetToNext)
        {
            GameController.instance.ChangeFacePart(facePart, true);
        }
        else if (transform.position.x - GameController.instance.top_initialPos.x < -Calibration.instance.dragOffsetToNext)
        {
            GameController.instance.ChangeFacePart(facePart, false);
        }
        else
        {
            GameController.instance.ResetPosition(facePart);
        }

        isDragging = false;
    }

    void Update()
    {
        if (isDragging)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePos.x, 0, 0);
        }
    }
}
